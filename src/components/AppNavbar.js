import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {BsFillCartFill} from 'react-icons/bs';
import '../Source.css';

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (


		<Navbar className="navbar navbar-expand-lg navbar-dark bg-primary">
		     <Container fluid>
		       <Navbar.Brand as={Link} to="/">Chapi</Navbar.Brand>
		       <Navbar.Toggle aria-controls="navbarScroll" />
		       <Navbar.Collapse id="navbarScroll">
		         <Nav className="ml-auto">
		           <Nav.Link as={Link} to="/">Home</Nav.Link>
		           { user.id 
			       ?
			       		user.isAdmin
			       			?
			       			<>
			       			<Nav.Link as={Link} to="/admin">Admin Dashboard</Nav.Link>
			       			<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
			       			</>
			       			:
			       			<>
			       			<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
			       			</>
			       	:
			       	<>
			       		<Nav.Link as={Link} to="/login">Login</Nav.Link>
			       		<Nav.Link as={Link} to="/viewProducts">Products</Nav.Link>
			       		<Nav.Link as={Link} to="/register">Register</Nav.Link>
			       	</>
			       }
		         </Nav>
		       </Navbar.Collapse>
		       <Nav id="navLeft" >
		       	<Nav.Link className="mr-4"><BsFillCartFill style={{width:'8rem'}}/></Nav.Link>
		       </Nav>
		     </Container>
		   </Navbar>
	)


	
}