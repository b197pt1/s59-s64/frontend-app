import { Form, Button, Container } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../Source.css'

export default function AddProduct(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState(0);

	const [isActive,setIsActive] = useState(true);

	useEffect(()=>{
		if(name !== "" && description !== "" && price !== 0){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	},[name, description, price]);

	function AddProduct(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch(`${process.env.REACT_APP_API_URL}/products/`,{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			name: name,
			description: description,
			price: price

			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Product Creation Failed.",
					text: data.message
				})	
				} 
			else {
				console.log(data)
				Swal.fire({
					icon: "success",
					title: "Product Creation Successful.",
					text: `Product has been created.`
				})
				navigate("/admin");
			}

		})
		setName("");
		setDescription("");
		setPrice(0);

	};

	return(
		<>
		<Container className="centerAdd">
			<h2 className = "text-center">Add Product</h2>
			<Form onSubmit={ e => AddProduct(e)}>
				<Form.Group controlId="name">
					<Form.Label>Product Name:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label>Description:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 
						type="number"
						placeholder="Enter Price"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>					

				{
					isActive 
					? <Button type="submit" variant="primary" className = "btnCrtProd">Submit</Button>
					: <Button type="submit" variant="btn btn-outline-primary" disabled className = "btnCrtProd">Submit</Button>
				}	
					<Button as={Link} to="/admin" variant="btn btn-outline-success" type="submit" className="btnCrtProd mx-2">
					Cancel
				</Button>
			</Form>
			</Container>
		</>
	)
}
