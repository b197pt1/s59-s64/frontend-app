import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const navigate = useNavigate();

	const [isActive, setIsActive] = useState(false);



	function loginUser (e) {

		
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			console.log(data.access);

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token',  data.access);
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Chapi!"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please, check your login details and try again."
				})
			}
		})

		setEmail("");
		setPassword("");
		navigate ('/viewProducts')
	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	};


	useEffect(() => {
		if(email !== "" && password !== "" )  {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])


	return(

		(user.id !== null) ?
			<Navigate to="/"/>
		:
			<Form onSubmit={(e) => loginUser(e)} >

				  <h1 className="text-center my-3">Login</h1>

			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value={email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter email" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password}
			        	onChange={(e) => {setPassword(e.target.value)}}
			        	placeholder="Enter Your Password" />
			      </Form.Group>
			      { isActive ?
			      			<Button variant="primary" type="submit" id="submitBtn">
			      		 	 Submit
			      			</Button>
			      			:
			      			<Button variant="danger" type="submit" id="submitBtn" disabled>
			      			  Submit
			      			</Button>
			      }
			     
			    </Form>
	)
}





// import { useState, useEffect, useContext } from 'react';
// import { Form, Button } from 'react-bootstrap';
// import { Navigate, useNavigate } from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';
// import '../Source.css';

// export default function Login() {

// 	const { user, setUser } = useContext(UserContext);

// 	const navigate = useNavigate();

// 	const [email, setEmail] = useState("");
// 	const [password, setPassword] = useState("");
// 	const [isActive, setIsActive] = useState(false);


// 	function loginUser(e) {

// 				e.preventDefault();

// 				fetch(`http://localhost:4000/users/login`, {
// 					method:"POST",
// 					headers: {
// 						'Content-Type': 'application/json'
// 					},
// 					body: JSON.stringify({
// 						email: email,
// 						password: password
// 					})
// 				})
// 				.then(res => res.json())
// 				.then(data => {

// 			console.log(data);
// 			console.log(data.access);

// 				if(data.accessToken){

// 					Swal.fire({
// 						icon: "success",
// 						title: "Login Successful!",
// 						text: `You are now logged in`
// 					})
// 					setEmail("")
// 					setPassword("")
// 					navigate ('/viewProducts')

// 					localStorage.setItem('token',data.accessToken);
					
// 					fetch(`http://localhost:4000/users/details`,{

// 						headers: {

// 							'Authorization': `Bearer ${data.accessToken}`

// 						}

// 					})
// 					.then(res => res.json())
// 					.then(data => {

// 						setUser({

// 							id: data._id,
// 							isAdmin: data.isAdmin

// 						})

// 					})

// 				} else {
// 					Swal.fire({
// 						icon: "error",
// 						title: "Login Failed",
// 						text: data.message
// 					})

// 				}
// 			})


// 		};



// 	useEffect(() => {

// 		if(email !== "" && password !=="") {
// 			setIsActive(true)
// 		} else {
// 			setIsActive(false)
// 		}
// 	}, [email, password])


// 		return (

// 			(user.id !== null)
// 				? <Navigate to="/"/>
// 				: 
// 				<>
// 					<h1 className="text-center mt-5">Login</h1>
// 					<Form onSubmit={(e) => loginUser(e)}>
// 					    <Form.Group className="mb-3" controlId="userEmail">
// 					       <Form.Label>Email address</Form.Label>
// 					       <Form.Control
// 					       		type="email"
// 					       		value={email}
// 					       		onChange={(e) => {setEmail(e.target.value)}}
// 					       		placeholder="Enter email" />
// 					    </Form.Group>

// 					    <Form.Group className="mb-3" controlId="userPassword">
// 					        <Form.Label>Password</Form.Label>
// 					        <Form.Control
// 					        	type="password"
// 					       		value={password}
// 					       		onChange={(e) => {setPassword(e.target.value)}}	        	
// 					        	placeholder="Enter Password" />
// 					    </Form.Group>
// 					    	{
// 					    		isActive
// 					    		? <Button variant="btn btn-outline-primary" type="submit" id="enrollBtn">Login</Button>
// 					    		: <Button variant="btn btn-outline-primary" type="submit" id="enrollBtn" disabled>Login</Button>
// 					    	}
// 					</Form>
// 				</>
// 		)

// 	}
